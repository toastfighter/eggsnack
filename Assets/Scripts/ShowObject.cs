﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowObject : MonoBehaviour
{
    [SerializeField]
    public GameObject appear;

    //[SerializeField]
    //public GameObject disappear;

    private void OnTriggerEnter(Collider other)
    {
        appear.SetActive(true);
        //disappear.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
