﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTrap : MonoBehaviour
{
    [SerializeField]
    public Animator trapAnimator;

    [SerializeField]
    public GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        trapAnimator.SetTrigger("Snap!");
        Debug.LogError("Entered Trigger");
    }
}
