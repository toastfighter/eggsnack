﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthChanger : MonoBehaviour
{
    public float currentHealth;
    public float maxHealth;
    public float damageToTake;

    public Slider healthbar;
    public GameObject DeathMassege;

    // Start is called before the first frame update
    void Start()
    {
       
        currentHealth = maxHealth;

        healthbar.value = calculatedHealth();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            TakeDamage(damageToTake);
        }
    }

    void TakeDamage(float dealedDamage)
    {
        currentHealth -= dealedDamage;
        healthbar.value = calculatedHealth();

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    float calculatedHealth()
    {
        return currentHealth / maxHealth;
    }

    void Die()
    {
        currentHealth = 0;
        DeathMassege.SetActive(true);
        Debug.Log("You´re Dead.");
    }
}
