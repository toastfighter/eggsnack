﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quiter : MonoBehaviour
{
    //Quits Game (should be linked with Quit-Button)
    public void QuitGame()
    {
        //Gets [Quit()] funktion out of UnityEngine Class [Application]
        Application.Quit();
        //shows in Unity if funtion works by clicking Button
        Debug.Log("Game Quit");
    }
}
