﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    public string sceneName;

    // Update is called once per frame
    public void LoadingScene()
    {
        SceneManager.LoadScene(sceneName);  
    }
}
