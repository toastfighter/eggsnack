﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    public string sceneName;
  
    public void LoadScene()
    {
     SceneManager.LoadScene(sceneName); 
    }
   
}
